source plot.lib.sh

plot "la fonction log en base 2" 1_000 <<EOF
let len = int_of_string Sys.argv.(1) in
for i = 0 to len do
   Printf.printf "%f\n" (log (float i) /. log 2.)
done
EOF