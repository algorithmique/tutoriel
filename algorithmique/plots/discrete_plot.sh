source plot.lib.sh

plot "nombre d'opérations de la dichotomie" 1_000 <<EOF
let len = int_of_string Sys.argv.(1) in
for i = 0 to len do
   Printf.printf "%f\n" (floor (log (float i) /. log 2.))
done
EOF