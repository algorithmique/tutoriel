function plot () {
    LEN=$1
    CODE=$2
    cat $CODE >tmp.ml

cat<<EOF>tmp.plot
plot "tmp.out" with dots
EOF

    ocaml tmp.ml $LEN > tmp.out
    gnuplot -persist tmp.plot

    for tmp in ml out plot
    do
        rm tmp.$tmp
    done
}
