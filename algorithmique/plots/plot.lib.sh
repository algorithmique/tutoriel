function plot () {
    TITLE=$1
    LEN=$2
    CODE=$3
    cat $CODE >tmp.ml

#no " in the title !
cat<<EOF>tmp.plot.sh
echo "plot 'tmp.out' with dots title \"$1\""
EOF

    ocaml tmp.ml $LEN > tmp.out
    sh tmp.plot.sh $TITLE > tmp.plot
    gnuplot -persist tmp.plot

    for tmp in ml out plot plot.sh
    do
        rm tmp.$tmp
    done
}
