#include <stdlib.h>
#include <stdio.h>

struct list
{
    int val;
    struct list *next;
};
typedef struct list List;

void free_list(List *list)
{
    while (list != NULL) {
        List *cell = list;
        list = cell->next;
        free(cell);
    }
}

List *cons(int valeur, List *liste)
{
    List *elem;
    if ((elem = malloc(sizeof *elem)) == NULL)
        return NULL;
    elem->val = valeur;
    elem->next = liste;
    return elem;
}


typedef List *Stack;

Stack *new_stack(void)
{
    Stack *stack;
    if ((stack = malloc(sizeof *stack)) == NULL)
        return NULL;
    *stack = NULL;
    return stack;
}

void free_stack(Stack *stack)
{
    free_list(*stack);
    free(stack);
}

int stack_is_empty(Stack *stack)
{
    return *stack == NULL;
}

int stack_push(Stack *stack, int elem)
{
    List *pushed;
    if ((pushed = cons(elem, *stack)) == NULL)
        return -1;
    *stack = pushed;
    return 0;
}

int stack_pop(Stack *stack, int *elem)
{
    List *tail;
    if (*stack == NULL)
        return -1;
    tail = (*stack)->next;
    *elem = (*stack)->val;
    free(*stack);
    *stack = tail;
    return 0;
}

#ifdef STACK
#define LEN 10

int main(void)
{
    int i;
    Stack *stack = new_stack();
    
    for (i = 0; i < LEN; ++i)
        stack_push(stack, i);
    while (!stack_is_empty(stack)) {
        int elem;
        stack_pop(stack, &elem);
        printf("%d\n", elem);
    }
    free_stack(stack);
    return 0;
}
#endif

struct queue
{
    List *input;
    List *output;
};

typedef struct queue Queue;

Queue *new_queue(void)
{
    Queue *queue;
    if ((queue = malloc(sizeof *queue)) == NULL)
        return NULL;
    queue->input = NULL;
    queue->output = NULL;
    return queue;
}

void free_queue(Queue *queue)
{
    free_list(queue->output);
    free(queue);
}

int queue_is_empty(Queue *queue)
{
    return queue->input == NULL;
}

int queue_push(Queue *queue, int elem)
{
    List *cell;
    if ((cell  = cons(elem, NULL)) == NULL)
        return -1;
    if (queue_is_empty(queue))
        queue->output = cell; /* output was NULL, set it to the single cell */
    else
        queue->input->next = cell;
    queue->input = cell;
    return 0;
}

int queue_pop(Queue *queue, int *elem) {
    List *cell;
    if ((cell = queue->output) == NULL)
        return -1;
    *elem = cell->val;
    queue->output = cell->next;
    if (queue->output == NULL) /* empty queue */
        queue->input = NULL;
    free(cell);
    return 0;
}

#ifndef STACK
#define LEN 10

int main(void)
{
    int i;
    Queue *queue = new_queue();
    
    for (i = 0; i < LEN; ++i)
        queue_push(queue, i);
    while (!queue_is_empty(queue)) {
        int elem;
        queue_pop(queue, &elem);
        printf("%d\n", elem);
    }
    free_queue(queue);
    return 0;
}
#endif
