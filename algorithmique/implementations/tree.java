class Tree<T> {
    T val;
    Tree<T>[] enfants;

    Tree(T val) {
        this.val = val;
        this.enfants = new Tree[0];
    }

    Tree(T val, Tree<T>[] enfants) {
        this.val = val;
        this.enfants = enfants;
    }

    public int taille() {
        int compteur = 1;
        for (Tree<?> enfant : enfants)
            compteur += enfant.taille();
        return compteur;
    }

    public static void main(String[] args) {
        Tree d = new Tree('D');
        Tree e = new Tree('E');
        Tree g = new Tree('G');
        Tree h = new Tree('H');
        Tree i = new Tree('I');

        Tree[] enfants_de_f = { g, h, i };
        Tree f = new Tree('F', enfants_de_f);

        Tree[] enfants_de_b = { d, e };
        Tree b = new Tree('B', enfants_de_b);

        Tree[] enfants_de_c = { f };
        Tree c = new Tree('C', enfants_de_c);

        Tree[] enfants_de_a = { b , c };
        Tree a = new Tree('A', enfants_de_a);

        System.out.println(a.taille());
    }
}