(** Définition des arbres généraux *)
type 'a arbre = Arbre of 'a * 'a arbre list

let exemple =
  let feuille lettre = Arbre (lettre, []) in
  Arbre ("A",
        [Arbre ("B", [feuille "D"; feuille "E"]);
         Arbre ("C", [Arbre ("F", [feuille "G"; feuille "H"; feuille "I"])])])

(** Quelques algorithmes *)
let rec taille (Arbre (_, enfants)) =
  let somme = List.fold_left (+) 0 in
  1 + somme (List.map taille enfants)

let rec hauteur (Arbre (_, enfants)) =
  let maxi = List.fold_left max 0 in
  1 + maxi (List.map hauteur enfants)

let rec liste (Arbre (elem, enfants)) =
  elem :: List.flatten (List.map liste enfants)


(** Parcours en profondeur *)
let rec dfs fonction (Arbre (elem, enfants)) =
  (* on donne deux éléments à la fonction de parcours :
     l'élément, et le résultat du parcours des enfants *)
  fonction elem (List.map (dfs fonction) enfants)

(* reformulation des algorithmes sous forme de parcours en profondeur *)
let taille a = dfs (fun _ t_enfants -> 1 + List.fold_left (+) 0 t_enfants) a
let hauteur a = dfs (fun _ h_enfants -> 1 + List.fold_left max 0 h_enfants) a
let liste a = dfs (fun elem l_enfants -> elem :: List.flatten l_enfants) a


(** Parcours en largeur *)

(* avec des couches *)
let bfs action arbre =
  let rec parcours_couche couche_courante couche_enfants =
    match couche_courante with
    | Arbre (element, enfants) :: nouvelle_couche_courante ->
        action element;
        let nouvelle_couche_enfants = enfants @ couche_enfants in
        parcours_couche nouvelle_couche_courante nouvelle_couche_enfants
    | [] ->
        print_endline "fin de couche";
        if couche_enfants <> [] then parcours_couche couche_enfants [] in
  parcours_couche [arbre] []

(* avec une file *)    
let bfs action arbre =
  let queue = Queue.create () in
  Queue.push arbre queue;
  while not (Queue.is_empty queue) do
    let Arbre (element, enfants) = Queue.pop queue in
    action element;
    List.iter (fun enfant -> Queue.push enfant queue) enfants
  done

(* en conservant la distance *)
let bfs action arbre =
  let rec parcours_couche distance couche_courante couche_enfants =
    match couche_courante with
    | Arbre (element, enfants) :: nouvelle_couche_courante ->
        action element distance;
        let nouvelle_couche_enfants = enfants @ couche_enfants in
        parcours_couche distance nouvelle_couche_courante nouvelle_couche_enfants
    | [] ->
        if couche_enfants <> []
        then parcours_couche (distance + 1) couche_enfants [] in
  parcours_couche 1 [arbre] []
