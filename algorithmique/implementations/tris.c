#include <stdlib.h>
#include <stdio.h>
 
struct list
{
    int val;
    struct list *next;
};
typedef struct list List;

void print_list(List *liste)
{
    while(liste != NULL) {
        printf("%d ", liste->val);
        liste = liste->next;
    }
}

List *cons(int valeur, List *liste)
{
    List *elem = malloc(sizeof(List));
    if (NULL == elem)
        exit(EXIT_FAILURE);
    elem->val = valeur;
    elem->next = liste;
    return elem;
}

/* 
  let rec retire_min min_actuel non_minimums = function
  | [] -> min_actuel, non_minimums
  | tete::queue ->
      (* on met le plus petit (min) comme minimum_actuel, et on rajoute le plus grand (max) dans les non-minimums *)
      retire_min (min min_actuel tete) (max min_actuel tete :: non_minimums) queue

 */
List retire_min(List *liste, List *non_mins, int min_actuel)
{
    if (NULL == liste) {
        List res;
        res.val = min_actuel;
        res.next = non_mins;
        return res;
    } else {
        int min = (liste->val < min_actuel ? liste->val : min_actuel);
        int max = (liste->val > min_actuel ? liste->val : min_actuel);
        return retire_min(liste->next, cons(max, non_mins), min);
    }
}

/*
  let rec tri_selection = function
  | [] -> []
  | tete::queue ->
      let plus_petit, reste =  retire_min tete [] queue in
      plus_petit :: tri_selection reste
 */
List *tri_selection(List *liste)
{
    if (NULL == liste)
        return NULL;
    else {
        List selection = retire_min(liste->next, NULL, liste->val);
        return cons(selection.val, tri_selection(selection.next));
    }
}

/* test :
int main(void)
{
    List *liste = cons(3,cons(2,cons(4,cons(1, cons(5, NULL)))));
    print_list(liste);
    printf("\n");
    List res = retire_min(liste->next, NULL, liste->val);
    print_list(res.next);
    printf("\t|\t%d\n", res.val);
    print_list(tri_selection(liste));
    printf("\n");
    return 0;
}
*/

/*
  let rec fusion = function
  | ([], li) | (li, []) -> li
  | tete_a::queue_a, tete_b::queue_b -> 
    if tete_a <= tete_b
    then tete_a :: fusion queue_a (tete_b::queue_b)
    else tete_b :: fusion (tete_a::queue_a) queue_b
 */
List *fusion(List *gauche, List *droite)
{
    if (NULL == gauche)
        return droite;
    if (NULL == droite)
        return gauche;
    if (gauche->val <= droite->val)
        return cons(gauche->val, fusion(gauche->next, droite));
    else
        return cons(droite->val, fusion(gauche, droite->next));                        
}

/* test :
int main(void)
{
    List *gauche = cons(1,cons(3,cons(5,NULL)));;
    List *droite = cons(2,cons(4,cons(4,NULL)));;
    print_list(fusion(gauche, droite));
    printf("\n");
    return 0;
}
*/

/*
  let rec decoupe = function
  | ([] | [_]) as liste -> (liste, [])
  | gauche::droite::reste ->
      let (reste_gauche, reste_droite) = decoupe reste in
      gauche :: reste_gauche, droite :: reste_droite
*/
void decoupe(List *liste, List **gauche, List **droite)
{
    do {
        if (NULL != liste) {
            *gauche = cons(liste->val, *gauche);
            liste = liste->next;
        }
        if (NULL != liste) {
            *droite = cons(liste->val, *droite);
            liste = liste->next;
        }
    } while (NULL != liste);
}

/*
  let rec tri_fusion = function
  | ([] | [_]) as liste_triee -> liste_triee
  | liste ->
    let demi_gauche, demi_droite = decoupe liste in
    fusion (tri_fusion demi_gauche, tri_fusion demi_droite)
*/

List *tri_fusion(List *liste)
{
    if (NULL == liste || NULL == liste->next)
        return liste;
    else {
        List *gauche = NULL, *droite = NULL;
        decoupe(liste, &gauche, &droite);
        return fusion(tri_fusion(gauche), tri_fusion(droite));
    }
}

/* test :
int main(void)
{
    List *liste = cons(3,cons(2,cons(4,cons(1, cons(5, NULL)))));
    print_list(liste);
    printf("\n");
    print_list(tri_fusion(liste));
    printf("\n");
    return 0;
}
*/
