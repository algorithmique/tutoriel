<?php

function arbre($val, $enfants)
{
    return array('val' => $val, 'enfants' => $enfants);
}

function taille($arbre)
{
    $count = 1;
    foreach($arbre['enfants'] as $enfant)
        $count += taille($enfant);
    return $count;
}

$arbre =
    arbre('A', array(
              arbre('B', array(
                        arbre('D', array()),
                        arbre('E', array()))),
              arbre('C', array(
                        arbre('F', array(
                                  arbre('G', array()),
                                  arbre('H', array()),
                                  arbre('I', array())))))));

echo taille($arbre) . "\n";

?>